//import DataStructure.ClientDataStructure;
//import DataStructure.ClientStructure;
//import ModelObject.Client;
//import io.qameta.allure.Epic;
//import io.qameta.allure.Story;
//import org.apache.poi.ss.formula.functions.T;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//@Epic("Тест на правильное поведение структуры данных")
//public class TestSizeAndClearClientStructure {
//    ClientDataStructure clientStructure = new ClientStructure();
//
//    @Story("Правильная очистка")
//    @Test
//    public void testCorrectClear() {
//        int startSize;
//        int sizeInMiddle;
//        int endSize;
//        int[] sizesEnd;
//        int[] sizes = new int[] {0, 1, 0}; // Нормальное поведение
//       // Client client = new Client();
//
//        //startSize = clientStructure.getSize();
//        //clientStructure.add(client);
//        sizeInMiddle = clientStructure.getSize();
//        clientStructure.clear();
//        endSize = clientStructure.getSize();
//        sizesEnd = new int[] {startSize, sizeInMiddle, endSize};
//
//        Assert.assertEquals(sizes, sizesEnd); // проверка что размеры на разных этапах совпадают в эталон.
//    }
//
//    @Story("Правильное определение размера")
//    @Test
//    public void testCorrectSize() {
//        clientStructure.add(new Client());
//        clientStructure.add(new Client());
//
//        Assert.assertTrue(clientStructure.getSize() == 2); // проверка что два элемента добавились
//    }
//
//    @Story("Два одинаковых элемента")
//    @Test
//    public void testCorrectTwoClone() {
//        Client client = new Client();
//
//        clientStructure.clear();
//        clientStructure.add(client);
//        clientStructure.add(client);
//
//        Assert.assertTrue(clientStructure.getSize() == 1); // проверка, что клон затёр добавленый ранее элемент
//    }
//}


