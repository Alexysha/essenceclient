import DataStructure.ClientDataStructure;
import DataStructure.ClientStructure;
import ModelObject.Client;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Random;

@Epic("Манипуляция данными в структуре")
public class TestManipulateOfStructure {
    private ClientDataStructure clientStructure = new ClientStructure();

    @DataProvider
    public Object[][] clientData() {
        return new Object[][]{
                {new Client("Дима", "Попов", "Попович"), true},
                {new Client("Жль", "Володин", "Ефримов"), true},
                {new Client("Маша", null, "Дмитриевна"), false},
                {new Client(null, "Иванов", "Иванович"), false},
                {new Client("Алексей", "Володин", "Ефримов"), true},
                {new Client(null, null, null), false},
                {new Client("Паша", "Сидоров", null), false},
                {null, false}
        };
    }

    @Story("Добавление клиентов в структуру")
    @Test(dataProvider = "clientData")
    public void testCorrectAdd(Client clientParam, boolean answer) {
        int startSize;
        int sizeAfterAdd;
        boolean correct;

        startSize = clientStructure.getSize(); // исходный размер
        correct = clientStructure.add(clientParam); // добовляем элемент
        Client client = clientStructure.find(clientParam); // пытаемся найти

        if (!answer) Assert.assertTrue(client == null); // Если клиент плохой, возвращает null
        else Assert.assertTrue(clientParam.equals(client)); // Иначе должен быть добавлен

        sizeAfterAdd = clientStructure.getSize(); // размер после добавления

        Assert.assertEquals(correct, answer); // проверка правильного добавления
        Assert.assertEquals(sizeAfterAdd - startSize == 1, answer); // проверка что размер увеличился на 1
    }


    @Story("Поиск клиента")
    @Test(dependsOnMethods = "testCorrectAdd", dataProvider = "clientData")
    public void testCorrectFind(Client clientParam, boolean answer) {
        boolean correctEquals;
        Client correctClient;

        correctClient = clientStructure.find(clientParam); // Поиск элемента
        /*
        Должен вернуть либо null в случае если объект не найден, либо объект должен быть
        равносильный объекту из параметра.
        */
        correctEquals = correctClient == null || clientParam.equals(correctClient);

        Assert.assertTrue(correctEquals); // проверка что объект один и тот же
    }


    @Story("Удаление клиента")
    @Test(dependsOnMethods = "testCorrectFind", dataProvider = "clientData")
    public void testCorrectDelete(Client clientParam, boolean answer) {
        int startSize;
        int endSize;

        startSize = clientStructure.getSize(); // исходный размер
        clientStructure.delete(clientParam); // находит элемент по имени
        endSize = clientStructure.getSize(); // Размер после удаления

        Assert.assertEquals(startSize - endSize == 1, answer); // проверка что размер уменьшился на 1
    }

    @Story("Размер и очистка")
    @Test(dependsOnMethods = "testCorrectDelete")
    public void testCorrectSizeAndClear() {
        clientStructure = new ClientStructure();
        Random random = new Random();
        Client client;
        int numAdditions;

        numAdditions = random.nextInt(50); // Заполняем рандомными значениями

        Assert.assertEquals(0, clientStructure.getSize()); // изначально размер = 0

        for (int i = 1; i <= numAdditions; i++) {
            String firstName = RandomStringUtils.randomAscii(10); // рандомное имя
            String lastName = RandomStringUtils.randomAscii(15); // рандомная фамилия
            String middleName = RandomStringUtils.randomAscii(20); // рандомное отчество

            client = new Client(firstName, lastName, middleName); // создание клиента
            clientStructure.add(client); // добавляем в бинарное дерево

            Assert.assertEquals(i, clientStructure.getSize()); // проверка что размер увеличился на i
        }
        clientStructure.clear(); // очистка

        Assert.assertEquals(0, clientStructure.getSize()); // после отчистки размер должен быть = 0
    }
}