package ModelObject;

public class Client implements Comparable {

    private String firstName;
    private String lastName;
    private String secondName;
    private String birthDay;
    private String passport;

    private Client() {
    }

    public Client(String firstName, String lastName, String secondName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    /**
     * Сравнение двух клиентов
     */
    @Override
    public boolean equals(Object obj) {
        boolean equalsName = true;
        boolean equalsPassport = true;

        if (obj == null)
            return false;
        if (!(obj instanceof Client))
            return false;

        final Client client = (Client) obj;

        if ((firstName == null) ?
                (client.firstName == null) : !firstName.equals(client.firstName))
            equalsName = false;

        if ((lastName == null) ?
                (client.lastName == null) : !lastName.equals(client.lastName))
            equalsName = false;

        if ((secondName == null) ?
                (client.secondName == null) : !secondName.equals(client.secondName))
            equalsName = false;

        if ((passport == null) ?
                (client.passport == null) : !passport.equals(client.passport))
            equalsPassport = false;

        return equalsName || equalsPassport;
    }

    @Override
    public int compareTo(Object o) {

        if (!(o instanceof Client) || o == null)
            return -1;

        Client client = (Client) o;

        if (firstName == null || client.firstName == null)
            return -1;

        return firstName.compareTo(client.firstName);
    }
}
