package DataStructure;

import ModelObject.Client;

public interface ClientDataStructure {
    boolean add(Client client);
    void delete(Client client);
    Client find(Client client);
    int getSize();
    void clear();
}