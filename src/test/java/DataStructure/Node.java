package DataStructure;

import ModelObject.Client;

public class Node {
    Client client;
    Node left;
    Node right;

    public Node(Client client) {
        this.client = client;
        left = null;
        right = null;
    }
}
