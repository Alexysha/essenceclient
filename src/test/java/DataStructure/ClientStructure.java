package DataStructure;

import ModelObject.Client;

public class ClientStructure implements ClientDataStructure {
    private Node root;
    int size = 0;

    @Override
    public boolean add(Client client) {
        if (checkNotNull(client)) {
            root = addRecursive(root, client);
            size++;
            return true;
        }
        return false;
    }

    @Override
    public void delete(Client client) {
        if (checkNotNull(client)) {
            root = deleteRecursive(root, client);
            size--;
        }
    }

    @Override
    public Client find(Client client) {
        if (client == null) return null;

        return containsNodeRecursive(root, client);
    }

    private Node addRecursive(Node current, Client client) {
        if (current == null)
            return new Node(client);

        if (client.compareTo(current.client) > 0) {
            current.left = addRecursive(current.left, client);
        } else if (client.compareTo(current.client) < 0) {
            current.right = addRecursive(current.right, client);
        } else {
            return current;
        }

        return current;
    }

    private Client containsNodeRecursive(Node current, Client client) {
        if (current == null)
            return null;

        if (client.compareTo(current.client) == 0)
            return current.client;

        return client.compareTo(current.client) > 0 ?
                containsNodeRecursive(current.left, client) :
                containsNodeRecursive(current.right, client);
    }

    private Node deleteRecursive(Node current, Client client) {
        if (current == null)
            return null;

        if (client.equals(current.client)) {

            if (current.left == null && current.right == null)
                return null;

            if (current.right == null) return current.left;
            if (current.left == null) return current.right;

            Client smallestValue = findSmallestValue(current.right);
            current.client = smallestValue;
            current.right = deleteRecursive(current.right, smallestValue);
            return current;
        }

        if (client.compareTo(current.client) > 0) {
            current.left = deleteRecursive(current.left, client);
            return current;
        }

        current.right = deleteRecursive(current.right, client);
        return current;
    }

    private Client findSmallestValue(Node root) {
        return root.left == null ? root.client : findSmallestValue(root.left);
    }

    /**
     * Проверяет на null объект клиента
     * @param client
     * @return true - если объект не null и его можно сравнить, false в пративном случае
     */
    private boolean checkNotNull(Client client) {
        boolean nameClient = true;
        boolean passportClient = true;

        if (client == null) return false;

        if (client.getFirstName() == null ||
                client.getLastName() == null ||
                client.getSecondName() == null) {
            nameClient = false;
        }

        if (client.getPassport() == null) {
            passportClient = false;
        }

        if (nameClient || passportClient)
            return true;

        return false;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }
}
