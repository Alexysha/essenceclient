//import ModelObject.Client;
//import io.qameta.allure.Epic;
//import io.qameta.allure.Feature;
//import io.qameta.allure.Story;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//
//@Epic("Тестирование корректной работы класса Client")
//public class TestCorrectClient {
//
//    @BeforeClass
//    public void tearDown() {
//    }
//
//    @Feature("Тест метода equals")
//    @Story("Тест одинаковых паспортных данных (отсутствие данных в других полях)")
//    @org.testng.annotations.Test
//    public void testEqualsPassport() {
//        Client client = new Client();
//        Client client2 = new Client();
//
//        client.setPassport("1");
//        client2.setPassport("1");
//
//        boolean equals = client.equals(client2);
//
//        Assert.assertTrue(equals);
//    }
//
//    @Feature("Тест метода equals")
//    @Story("Тест двух разных объектов")
//    @org.testng.annotations.Test
//    public void testEqualsNull() {
//        Client client = new Client();
//        Client client2 = new Client();
//
//        boolean equals = client.equals(client2);
//
//        Assert.assertFalse(equals);
//    }
//
//    @Feature("Тест метода equals")
//    @Story("Тест объектов, у одного их которых отсутствует фамилия")
//    @org.testng.annotations.Test
//    public void testEqualsName() {
//        Client client = new Client();
//        Client client2 = new Client();
//
//        client.setFirstName("Дмитрий");
//        client.setLastName("Сидоров");
//        client.setSecondName("Сидорович");
//
//        client2.setFirstName("Дмитрий");
//        client2.setSecondName("Сидорович");
//
//        boolean equals = client.equals(client2);
//
//        Assert.assertFalse(equals);
//    }
//
//    @Feature("Тест метода equals")
//    @Story("Тест объектов, у одного из которых отсутствует имя, а у другого фамилия")
//    @org.testng.annotations.Test
//    public void testEqualsName_2() {
//        Client client = new Client();
//        Client client2 = new Client();
//
//        client.setLastName("Сидоров");
//        client.setSecondName("Сидорович");
//
//        client2.setFirstName("Дмитрий");
//        client2.setSecondName("Сидорович");
//
//        boolean equals = client.equals(client2);
//
//        Assert.assertFalse(equals);
//    }
//
//    @Feature("Тест метода equals")
//    @Story("Тест объектов, с разным ФИО, но с одинаковыми паспортами")
//    @org.testng.annotations.Test
//    public void testEqualsName_3() {
//        Client client = new Client();
//        Client client2 = new Client();
//
//        client.setFirstName("Дмитрий");
//        client.setLastName("Сидоров");
//        client.setSecondName("Сидорович");
//        client.setPassport("1");
//
//        client2.setFirstName("Дмитрий");
//        client2.setSecondName("Сидорович");
//        client2.setPassport("1");
//
//        boolean equals = client.equals(client2);
//
//        Assert.assertTrue(equals);
//    }
//
//    @Feature("Тест метода equals")
//    @Story("Тест двух объектов с одинаковым ФИО и паспортами")
//    @org.testng.annotations.Test
//    public void testEqualsName_4() {
//        Client client = new Client();
//        Client client2 = new Client();
//
//        client.setFirstName("Дмитрий");
//        client.setLastName("Сидоров");
//        client.setSecondName("Сидорович");
//        client.setPassport("1");
//
//        client2.setFirstName("Дмитрий");
//        client2.setLastName("Сидоров");
//        client2.setSecondName("Сидорович");
//        client2.setPassport("1");
//
//        boolean equals = client.equals(client2);
//
//        Assert.assertTrue(equals);
//    }
//
//    @Feature("Тест метода equals")
//    @Story("Тест объектов с одинаковым ФИО")
//    @org.testng.annotations.Test
//    public void testEqualsName_5() {
//        Client client = new Client();
//        Client client2 = new Client();
//
//        client.setFirstName("Дмитрий");
//        client.setLastName("Сидоров");
//        client.setSecondName("Сидорович");
//
//        client2.setFirstName("Дмитрий");
//        client2.setLastName("Сидоров");
//        client2.setSecondName("Сидорович");
//
//        boolean equals = client.equals(client2);
//
//        Assert.assertTrue(equals);
//    }
//}
