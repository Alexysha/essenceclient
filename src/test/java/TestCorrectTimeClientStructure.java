//import DataStructure.ClientStructure;
//import ModelObject.Client;
//import io.qameta.allure.Epic;
//import io.qameta.allure.Story;
//import org.apache.commons.lang3.RandomStringUtils;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//
//import java.util.concurrent.TimeUnit;
//
//@Epic("Тест на производительность структуры данных")
//public class TestCorrectTimeClientStructure {
//    private ClientStructure clientStructure;
//    private Client client;
//    private Client findClient;
//
//    @Story("Время на добавления")
//    @Test
//    public void testTimeAdd() {
//        long endTime;
//        clientStructure = new ClientStructure();
//        long startTime = System.nanoTime();
//
//        for (int i = 0; i < 2_000_000; i++) {
//            if (i == 555_555) {
//                 findClient = client;
//            }
//            client = new Client();
//            client.setFirstName(RandomStringUtils.randomAscii(100));
//            clientStructure.add(client);
//        }
//
//        endTime = System.nanoTime() - startTime;
//
//        System.out.println("Добавление элементов за: " +
//                TimeUnit.NANOSECONDS.toSeconds(endTime) + " сек.");
//    }
//
//    @Story("Время на поиск")
//    @Test(dependsOnMethods = "testTimeAdd")
//    public void testTimeFind() {
//        long endTime;
//        long startTime = System.nanoTime();
//
//        if (clientStructure.find(findClient) == null) {
//            System.out.println("Элемент не найден");
//            return;
//        }
//
//        endTime = System.nanoTime() - startTime;
//
//        System.out.println("Поиск элемента за: " +
//                TimeUnit.NANOSECONDS.toMillis(endTime) + " милисек.");
//    }
//
//    @Story("Время на удаление")
//    @Test(dependsOnMethods = "testTimeFind")
//    public void testTimeDelete() {
//        long endTime;
//        long startTime = System.nanoTime();
//
//        if (!clientStructure.delete(findClient)) {
//            System.out.println("Неудачное удаление");
//            return;
//        }
//
//        endTime = System.nanoTime() - startTime;
//
//        System.out.println("Удаление элемента за: " +
//                TimeUnit.NANOSECONDS.toMicros(endTime) + " наносек.");
//    }
//}
